/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */

"use strict";

!function($){
	
	$(document).ready(function(){

		var navs = $('[data-nav="accordion"]');

		navs.each(function(){
			
			var base = $(this);
			var parents = base.find('.parent');
			var items = base.find('[class^="item-"]:not(.parent)');
parents.removeClass('open');
			items.on('click', function(event){ 
				event.stopPropagation();
			});

			parents.each( function() {

				var parent = $(this);
				
				if (parent.hasClass('active')) {
					var navChild = parent.find('> .nav-child');
					navChild.addClass('open');					
				}

				parent.on('click', function(event){

					event.preventDefault();
				
					var item = $(this);
					var navChild = item.find('> .nav-child');
					if (navChild.hasClass('open')) {
						TweenMax.to(navChild, 0.5, {height: 0, ease: Expo.easeOut});
						navChild.removeClass('open');
						item.removeClass('listaAberta');
					} else {
						TweenMax.set(navChild, {height: 'auto', maxHeight: 'none'});
						TweenMax.from(navChild, 0.5, {height: 0});
						navChild.addClass('open');
						item.addClass('listaAberta');
					};

					return false;

				});

			});

		});

	});

}(jQuery);

